/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright(c) 2010-2016 Intel Corporation
 */

#ifndef _MAIN_H_
#define _MAIN_H_

#ifndef APP_MAX_PORTS
#define APP_MAX_PORTS 4
#endif

struct app_params {
	/* CPU cores */
	uint16_t lcores[APP_MAX_PORTS]; /* lcore per port */
	uint16_t n_lcores;

	/* Ports*/
	uint16_t ports[APP_MAX_PORTS];
	uint16_t n_ports;

	/* Rings */
	struct rte_ring *rings_tx[APP_MAX_PORTS];
	uint16_t ring_tx_size;

	/* Buffer pool */
	struct rte_mempool *pool;
	uint16_t pool_buffer_size;
	uint16_t pool_size;
	uint16_t pool_cache_size;

	/* Ethernet addresses */
	struct ether_addr port_eth_addr[APP_MAX_PORTS];
	struct ether_addr next_hop_eth_addr[APP_MAX_PORTS];
} __rte_cache_aligned;

extern struct app_params app;

int app_parse_args(int argc, char **argv);
void app_print_usage(void);
void app_init(void);
int app_lcore_main_loop(void *arg);

void app_main_loop(uint16_t port);
void handle_tx(uint16_t port);

#define APP_FLUSH 0
#ifndef APP_FLUSH
#define APP_FLUSH 0x3FF
#endif

#define APP_METADATA_OFFSET(offset) (sizeof(struct rte_mbuf) + (offset))

#endif /* _MAIN_H_ */
