## **Build**

- Build and configure DPDK from source (see slides)
- Run `make` inside this directory
- Add hardware addresses of next hop interfaces connected to the switch, to `next_hop.txt`. The order corresponds to port index (e.g., line 1 -> port 0).

## **Run**

- `sudo ./switch -l 0,1 -- -p 0x3 -n next_hop.txt`