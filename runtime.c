/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright(c) 2010-2014 Intel Corporation
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <sys/types.h>
#include <string.h>
#include <sys/queue.h>
#include <stdarg.h>
#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <sys/time.h>
#include <math.h>

#include <rte_common.h>
#include <rte_byteorder.h>
#include <rte_log.h>
#include <rte_memory.h>
#include <rte_memcpy.h>
#include <rte_eal.h>
#include <rte_per_lcore.h>
#include <rte_launch.h>
#include <rte_atomic.h>
#include <rte_cycles.h>
#include <rte_prefetch.h>
#include <rte_branch_prediction.h>
#include <rte_interrupts.h>
#include <rte_pci.h>
#include <rte_random.h>
#include <rte_debug.h>
#include <rte_ether.h>
#include <rte_ethdev.h>
#include <rte_ring.h>
#include <rte_mempool.h>
#include <rte_mbuf.h>
#include <rte_ip.h>
#include <rte_tcp.h>
#include <rte_icmp.h>
#include <rte_udp.h>
#include <rte_malloc.h>

#include "main.h"

#ifndef APP_MBUF_ARRAY_SIZE
#define APP_MBUF_ARRAY_SIZE 1024
#endif

static int
get_exit_port(const struct rte_mbuf *mbuf)
{
        int dst_port = -1;

        if (RTE_ETH_IS_IPV4_HDR(mbuf->packet_type)) {
                struct ipv4_hdr *ipv4_hdr;
                struct ether_hdr *eth_hdr;
                uint8_t subnet;

                eth_hdr = rte_pktmbuf_mtod(mbuf, struct ether_hdr *);
                ipv4_hdr = (struct ipv4_hdr *)(eth_hdr + 1);

                subnet = (uint8_t)(rte_be_to_cpu_32(ipv4_hdr->dst_addr) >> 8);
        
		dst_port = subnet == 2 ? 1 : 0;
	}

        return dst_port;
}

static void
forward_msg(struct rte_mbuf *mbuf)
{
        int ret;
        int dst_port;

        dst_port = get_exit_port(mbuf);
        if (dst_port == -1) {
                rte_pktmbuf_free(mbuf);
        } else {
                do {
                        ret = rte_ring_mp_enqueue(app.rings_tx[dst_port], (void *)mbuf);
                } while (ret == -ENOBUFS);
        }
}

void
app_main_loop(uint16_t port) {
	RTE_LOG(INFO, USER1, "Core %u is receiving from port %u\n",
			rte_lcore_id(), port);

	struct rte_mbuf *mbuf_array[APP_MBUF_ARRAY_SIZE];

	while (1) {
		uint16_t n_read;
		uint16_t i;

		/* Read from port */
		n_read = rte_eth_rx_burst(app.ports[port], 0, mbuf_array, APP_MBUF_ARRAY_SIZE);

        	/* Route messages */
        	for (i = 0; i < n_read; i++)
                	forward_msg(mbuf_array[i]);
		
		/* Handle Tx */
		handle_tx(port);
	}
}

static void
update_mac(struct rte_mbuf *mbuf,
                uint16_t port)
{
        struct ether_hdr *eth;

        eth = rte_pktmbuf_mtod(mbuf, struct ether_hdr *);

        /* s_addr = MAC of the dest port */
        ether_addr_copy(&app.port_eth_addr[port], &eth->s_addr);

        /* d_addr = MAC of the next hop */
        ether_addr_copy(&app.next_hop_eth_addr[port], &eth->d_addr);
}

static uint16_t
eth_tx_pkt(uint16_t port, struct rte_mbuf *mbuf)
{
        update_mac(mbuf, port);

        return rte_eth_tx_burst(app.ports[port], 0, &mbuf, 1);
}

void
handle_tx(uint16_t port) {
	struct rte_mbuf *mbuf_array[APP_MBUF_ARRAY_SIZE];
	unsigned n_read;
	uint16_t i;

	/* Read */
	n_read = rte_ring_sc_dequeue_burst(
			app.rings_tx[port], (void **)mbuf_array, APP_MBUF_ARRAY_SIZE, NULL);
	
	/* Send */
	for (i = 0; i < n_read; i++) {
		struct rte_mbuf *mbuf = mbuf_array[i];
		if (!eth_tx_pkt(port, mbuf))
			break;
	}
}
